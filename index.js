/*
	Activity:

	1. Update and Debug the following codes to ES6.
		Use template literals,
		Use array/object destructuring,
		Use arrow functions
	
    2. Create a class constructor able to receive 3 arguments
        It should be able to receive 3 strings and a number
        Using the this keyword assign properties:
        username, 
        role, 
        guildName,
        level 
  assign the parameters as values to each property.
  Create 2 new objects using our class constructor.
  This constructor should be able to create Character objects.



	Create 2 new objects using our class constructor.

	This constructor should be able to create Dog objects.

	Log the 2 new Dog objects in the console.


	Pushing Instructions:

	Create a git repository named S24.
	Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
	Add the link in Boodle.

*/

//Solution: 
class Player{
	constructor(username,role,guildName,level,){
		this.Name=username;
		this.class=role;
		this.guild=guildName;
		this.level=level;
	}
}
let Tanjiro=new Player("Kamado Tanjiro","Leader","Demon Hunters",50);
let Zinetsu=new Player("Zenitsu","Clown","Demon Hunters",45);


console.log(Tanjiro);
console.log(Zinetsu);



/*Debug*/
let student1 = {
	name: "Shawn Michaels",
	birthday: "May 5, 2003",
	age: 18,
	isEnrolled: true,
	classes: ["Philosphy 101","Social Sciences 201"],
};


let student2 = {
	name: "Steve Austin",
	birthday: "June 15, 2001",
	age: 20,
	isEnrolled: true,
	classes: ["Philosphy 401", "Natural Sciences 402"],
};


/*Debug and Update*/

const introduce=(student)=>{

	console.log(`"Hi! I'm " ${student.name} "." + "I am " ${student.age}" years old."`);
	console.log(`"I study the following courses:" ${student.class}`);

};



introduce(student1);
introduce(student2);

function getCube(num){

	/*console.log(Math.pow(num,3))*/
	let numCube=num**3;
	console.log(numCube);

};
getCube(12);


let numArr = [15,16,32,21,21,2]

numArr.forEach((num)=>{
	console.log(num.length);
}
);
let items=[1];

items.forEach((item)=>{
	let newItem=item;
	console.log(item);

let numsSquared = numArr.map((num)=>num**2);
	console.log(numsSquared);
	



})  
